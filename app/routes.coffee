# app/routes.coffee -> app/routes.js

module.exports = (app) ->
    # Rutas del servidor =======================================
    # Manejamos cosas como llamadas a la API
    # o rutas de autenticación

    app.get '/', (req, res) ->
        res.render 'index.jade'

    app.get '/*', (req, res) ->
        res.render 'index.jade'
