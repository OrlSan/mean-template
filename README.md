# Plantilla para aplicaciones MEAN
Evitando la configuración manual de cada archivo nuevo en una nueva aplicación podemos ahorrar mucho tiempo.

Los pasos a seguir son:

* Clonar este repositorio, recomendablemente con otro nombre en la carpeta
* Instalar los paquetes necesarios con **npm**. Verificar si hacen falta paquetes a instalar con ```npm install```
* Instalar las dependencias nuevas con Bower (verificar el archivo bower.json para ver si lo que requieres está ahí)
```
bower install
```
* Cambiar el origen del repositorio, quitando la dirección Git de donde se obtuvo.
```
git remote set-url origin git://new.url.here
```

¡Eso es todo!