###
# Plantilla para aplicaciones web MEAN:
# Mongo - Base de datos
# Express.js - Framework para Node.js
# Angular.js - Front-end framework
# Node.js - Plataforma para construir aplicaciones
###

# Cargamos los módulos necesarios
express = require 'express'
app = express()
mongoose = require 'mongoose'
logger = require 'morgan'
bodyParser = require 'body-parser'
# Configuración =======================================
# Base de datos
db = require './config/db'

port  = process.env.PORT || 8080
# Descomentar hasta que hayamos configurado el archivo
mongoose.connect db.url

app.use express.static(__dirname + '/public')
app.use '/js', express.static(__dirname + '/scripts/js')
app.use logger('dev')
app.use bodyParser()

# Rutas ===============================================
require('./app/routes')(app)

# Iniciar la aplicación ==============================
app.listen port
console.log 'Escuchando en el puerto ' + port
exports = module.exports = app
