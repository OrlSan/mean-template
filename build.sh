echo 'Compilando la aplicación...'

# Servidor principal
coffee -c -b server.coffee
# Archivos de la aplicación
coffee -c -b app/*.coffee
# Archivos .js para el lado del cliente
coffee -o scripts/js/ -c -b scripts/*.coffee
# Archivos de configuraciones
# coffee -c -b config/*.coffee

echo 'Compilación terminada'
